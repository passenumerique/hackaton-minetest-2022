controls.register_on_press(function(player, control_name)
    if ( control_name == "jump") then
        name = player:get_player_name() 
        minetest.chat_send_player(name, "Bien joué")
    end
end)

controls.register_on_release(function(player, control_name, time)
    -- called on key-release
    -- control_name: see above
    -- time: seconds the jey was pressed
end)

controls.register_on_hold(function(player, control_name, time)
    -- called every globalstep if the key is pressed
    -- control_name: see above
    -- time: seconds the key was pressed
end)