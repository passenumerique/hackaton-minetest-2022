    minetest.register_craftitem("zelda:breath", { 
    description = "Ceci est un item pour respirer sous l'eau",
    inventory_image = "breath.png",
            

    -- Définir la fonction --

    on_use = function(itemstack, user, pointed_thing)
        local player = user:set_breath(10)

        end

    })
    
    minetest.register_node("zelda:barriere", {
        description = "barriere",
        tiles = {"barrierezelda.png",
                },
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("zelda:buisson", {
        description = "buisson",
        tiles = {"buissonzelda.png",
                },
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("zelda:chemin1", {
        description = "chemin1",
        tiles = {"chemin1zelda.png",
                },
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("zelda:chemin2", {
        description = "chemin2",
        tiles = {"chemin2zelda.png",
                },
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("zelda:entree", {
        description = "entree",
        tiles = {"entreezelda.png",
                },
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("zelda:grotte", {
        description = "grotte",
        tiles = {"grottezelda.png",
                },
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("zelda:herbe", {
        description = "herbe",
        tiles = {"herbezelda.png",
                },
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("zelda:herbe2", {
        description = "herbe2",
        tiles = {"herbe2zelda.png",
                },
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("zelda:herbe3", {
        description = "herbe3",
        tiles = {"herbe3zelda.png",
                },
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("zelda:herbe4", {
        description = "herbe4",
        tiles = {"herbe4zelda.png",
                },
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("zelda:herbe5", {
        description = "herbe5",
        tiles = {"herbe5zelda.png",
                },
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("zelda:herbe6", {
        description = "herbe6",
        tiles = {"herbe6zelda.png",
                },
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("zelda:herbe7", {
        description = "herbe7",
        tiles = {"herbe7zelda.png",
                },
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("zelda:herbe8", {
        description = "herbe8",
        tiles = {"herbe8zelda.png",
                },
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("zelda:mur", {
        description = "mur",
        tiles = {"murzelda.png",
                },
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("zelda:pont", {
        description = "pont",
        tiles = {"pontzelda.png",
                },
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("zelda:terre", {
        description = "terre",
        tiles = {"terrezelda.png",
                },
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("zelda:toit", {
        description = "toit",
        tiles = {"toitzelda.png",
                },
    
        groups = { oddly_breakable_by_hand= 1},   
    })
