    minetest.register_craftitem("spyro:flyblock", { 
    description = "Ceci est un item pour graviter",
    inventory_image = "cubeflyspyro.png",

    -- Définir la fonction --

    on_use = function(itemstack, user, pointed_thing)
        local playergravity = user:get_physics_override().gravity

        if ( playergravity < 1) then
            user:set_physics_override({
            gravity = 1,
        })
        else
            user:set_physics_override({
                gravity = 0.1,
        })
        end

        end,
    })


    minetest.register_node("spyro:buisson", {
        description = "buisson",
        tiles = {"buissonspyro.png",
                },
    
        -- Casser le bloc --
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("spyro:herbe", {
        description = "herbe",
        tiles = {"herbespyro.png",
                },
    
        -- Casser le bloc --
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("spyro:chemin", {
        description = "chemin",
        tiles = {"cheminspyro.png",
                 "herbespyro.png",
                 "herbespyro.png",
                 "herbespyro.png",
                 "herbespyro.png",
                 "herbespyro.png",
                },
    
        -- Casser le bloc --
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("spyro:chateau", {
        description = "chateau",
        tiles = {"blocchateauspyro.png",
                },
    
        -- Casser le bloc --
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("spyro:cielbas", {
        description = "cielbas",
        tiles = {"cielbasspyro.png",
                },
    
        -- Casser le bloc --
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("spyro:cielhaut", {
        description = "cielhaut",
        tiles = {"cielhautspyro.png",
                },
    
        -- Casser le bloc --
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_tool("spyro:bouledefeu",{
        description = "bloc qui met le feu",
        inventory_image ="feuspyro.png",
        on_use = function(itemstack, user, pointed_thing)
            local pointer = pointed_thing
            if pointer.above == nil then
                return (nil)
            else 
                minetest.set_node(pointer.above, {name = "fire:basic_flame"})
            end
        end
    })

    minetest.register_node("spyro:gemme", {
        description = "gemme",
        tiles = {"gemme1spyro.png",
                 "gemme2spyro.png",
                 "gemme3spyro.png",
                 "gemme4spyro.png",
                 "gemme5spyro.png",
                },
    
        -- Casser le bloc --
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("spyro:montagne1", {
        description = "montagne",
        tiles = {"montagne1spyro.png",
                },
    
        -- Casser le bloc --
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("spyro:montagne2", {
        description = "montagne",
        tiles = {"montagne2spyro.png",
                },
    
        -- Casser le bloc --
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("spyro:terre", {
        description = "terre",
        tiles = {"terrespyro.png",
                },
    
        -- Casser le bloc --
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("spyro:toit", {
        description = "toitchateau",
        tiles = {"toitchateauspyro.png",
                },
    
        -- Casser le bloc --
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("spyro:fenetre1", {
        description = "fenetre",
        tiles = {"windowleftupspyro.png",
                },
    
        -- Casser le bloc --
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("spyro:fenetre2", {
        description = "fenetre",
        tiles = {"windowleftdownspyro.png",
                },
    
        -- Casser le bloc --
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("spyro:fenetre3", {
        description = "fenetre",
        tiles = {"windowrightupspyro.png",
                },
    
        -- Casser le bloc --
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("spyro:fenetre4", {
        description = "fenetre",
        tiles = {"windowrightdownspyro.png",
                },
    
        -- Casser le bloc --
    
        groups = { oddly_breakable_by_hand= 1},   
    })


