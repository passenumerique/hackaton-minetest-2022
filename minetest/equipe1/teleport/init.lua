minetest.register_node("teleport:telsonic", {
    description = "Ceci est un bloc pour se téléporter vers le monde sonic",
    tiles = {"teleport3.png"
            },

    -- Casser le bloc --

    groups = { oddly_breakable_by_hand= 1},   

    -- Définir la fonction du bloc --

    on_punch = function(pos, node, puncher, pointed_thing)
        
        local_player= puncher:move_to({ x = 75.0, y= 8.5, z = 95 })

        end
    })

    minetest.register_node("teleport:telspyro", {
        description = "Ceci est un bloc pour se téléporter vers le monde spyro",
        tiles = {"teleport.png"
                },
    
        -- Casser le bloc --
    
        groups = { oddly_breakable_by_hand= 1},   
    
        -- Définir la fonction du bloc --
    
        on_punch = function(pos, node, puncher, pointed_thing)
            
            local_player= puncher:move_to({ x = 83.2, y= 15.5, z = 113.2 })
    
            end
        })

        minetest.register_node("teleport:telzelda", {
            description = "Ceci est un bloc pour se téléporter vers le monde zelda",
            tiles = {"teleport2.png"
                    },
        
            -- Casser le bloc --
        
            groups = { oddly_breakable_by_hand= 1},   
        
            -- Définir la fonction du bloc --
        
            on_punch = function(pos, node, puncher, pointed_thing)
                
                local_player= puncher:move_to({ x = 67.1, y= 30.5, z = 96.7})
        
                end
            })