minetest.register_node("sonic:speedblock", {
    description = "Ceci est un bloc pour aller vite",
    tiles = {"sidesonic.png"
            },

    -- Casser le bloc --

    groups = { oddly_breakable_by_hand= 1},   

    -- Définir la fonction du bloc --

    on_punch = function(pos, node, puncher, pointed_thing)
        local playerspeed = puncher:get_physics_override().speed

            if playerspeed > 1 then
                puncher:set_physics_override({
                speed = 1,
            })
            elseif playerspeed == 1 then
                puncher:set_physics_override({
                    speed = 10,
            })
            end

        end,
    })

    minetest.register_node("sonic:herbe", {
        description = "herbe",
        tiles = {"herbesonic.png",
                "herbe2sonic.png",
                "herbe2sonic.png",
                "herbe2sonic.png",
                "herbe2sonic.png",
                "herbe2sonic.png"
                },
    
        -- Casser le bloc --
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("sonic:sol", {
        description = "sol",
        tiles = {"solsonic.png",
                },
    
        -- Casser le bloc --
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("sonic:ciel", {
        description = "ciel",
        tiles = {"cielsonic.png",
                },
    
        -- Casser le bloc --
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    minetest.register_node("sonic:piege", {
        description = "piege",
        tiles = {"piege2sonic.png",
                 "piegesonic.png",
                 "piegesonic.png",
                 "piegesonic.png",
                 "piegesonic.png",
                 "piegesonic.png",
                },
    
        -- Casser le bloc --
    
        groups = { oddly_breakable_by_hand= 1}, 

        -- Définir la fonction --
    
        on_punch = function(pos, node, puncher, pointed_thing)
            
            local player = puncher:set_hp(0)
    
        end
    })

    
        
    minetest.register_node("sonic:anneau", {
        description = "anneau",
        tiles = {"anneausonic.png",
                },
    
        -- Casser le bloc --
    
        groups = { oddly_breakable_by_hand= 1},   
    })

    