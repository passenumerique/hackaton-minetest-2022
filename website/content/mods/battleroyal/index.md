---
title: "Battle Royal"
image: "bloc_darme.jpeg"
image_text: "Cette image montre le bloc d'arme aléatoire"
image2: "bloc_darmure"
image2_text: "Cette image montre le bloc d'armure aléatoire"
tags: [Battle Royal,aléatoire,PVP]
description: "Ce mod propose des des blocs contenant des armes et armures aléatoire pour équiper les joueurs au début d'un tournoi PVP"
---