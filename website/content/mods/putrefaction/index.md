---
title: "Putréfaction"
image: "bloc.jpeg"
image_text: "Cette image montre le bloc d'herbe magique"
tags: [gore,magic]
description: "Ce mod propose des plantes qui putréfient lentement le joueur qui les mange"
equipe: 7
---