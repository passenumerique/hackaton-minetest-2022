---
title: "DevOps"
description: "Des ressources pour les outils devops de déploiement du site"
menu: "main"
weight: "100"
layout: "resources_single.html"
---

Pour mettre à jour correctement le projet, il faut utiliser la ligne de commandes dans Visual Studio Code.

1. Mettre à jour le projet depuis le cloud:  
  ```
  git checkout main
  git pull
  ```
2. Intégrer les changements dans la branche de l'équipe
   ```
   git checkout equipe27
   git merge main
   ```
3. Pousser les changements de la branche d'équipe vers le cloud
   ```
   git push
   ```