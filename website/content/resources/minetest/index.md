---
title: "Minetest"
description: "Des ressources pour coder dans Minetest"
menu: "main"
weight: "200"
---

# Consignes

Vous devez réaliser un mod basé sur la création d'un nouveau bloc.

Pour cela vous devez créer un dossier portant le nom de votre équipe dans le dossier minetest.

Ensuite vous devez créer un lien entre le dossier mods de votre installation de Minetest et le dossier que vous aurez créé.

Dans ce dossier, vous créez 2 fichiers: 
  - init.lua, qui contient le code
  - mod.conf, qui contient les informations sur le mod.

# Ressources

Pour coder votre mod Minetest, vous pourrez vous aider des ressources suivantes:  
  - la [documentation complète de l'API](https://garagenum.gitlab.io/minetest-api-doc/)
  - les [tutos de Nathan Salapat](https://www.nathansalapat.com/index.php/minetest/modding?page=1)