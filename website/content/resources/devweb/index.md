---
title: "DevWeb"
description: "Des ressources pour coder la page web"
menu: "main"
weight: "200"
layout: "resources_single.html"
---

Pour développer la page web de présentation, vous devez créer un dossier avec le nom de votre équipe dans `website/content/mods/`

Dans ce dossier, vous devez créer un fichier `index.md` en vous inspirant du fichier `index.md` dans `website/content/mods/putrefaction`. Les informations contenus dans ce fichier viendront remplir la petite carte de la page d'accueil.

Vous pouvez ensuite créer votre page complète `index.html` dans le même dossier.