# Hackaton Minetest 2022

## Description
Ce projet sert de support au hackaton Minetest réalisé en novembre 2022 dans le cadre du Passe numérique.

## Contenu

Ce projet contient 4 dossiers principaux:
- `ressources`, qui contient tous les documents et liens nécessaires à la réalisation du projet
- `minetest`, qui contient les mods réalisés pour Minetest
- `website`, qui contient le code-source du site web de présentation des mods
- `communication`, qui ocntient les documents associés à la présentation du mode

Dans chacun des dossiers `mods`, `website`, `communication`, il y a un dossier pour chaque projet de mod.

L'arborescence du projet ressemble à ça (avec deux mods "flash" et "destruction"):

```
--- hackaton-minetest-2022:
    |
    +--- ressources:
    |    |
    |    +-- minetest
    |    |
    |    +-- website
    |    |
    |    +-- communication
    |
    +--- minetest:
    |    |
    |    +-- flash
    |    |
    |    +-- destruction
    |    |
    |    +-- ...
    |
    +-- website:
    |    |
    |    +-- flash
    |    |
    |    +-- destruction
    |    |
    |    +-- ...
    |
    +-- communication:
        |
        +-- flash
        |
        +-- destruction
        |
        +-- ...
```

## Utilisation

1. Installer git, Visual Studio Code ( ou Codium ), et Hugo
2. Cloner ce projet sur votre poste de travail
```
git clone https://gitlab.com/passenumerique/hackaton-minetest-2022.git
cd hackaton-minetest-2022
```
3. Créer une nouvelle branche du projet avec votre nom de mod

Par exemple, si votre mod s'appelle "Panique au Moyen Âge":
```
git checkout -b panique-au-moyen-age
```

4. Créer les dossiers nécessaires

Avec le même exemple:
```
mkdir minetest/panique-au-moyen-age communication/panique-au-moyen-age website/panique-au-moyen-age
```


## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
